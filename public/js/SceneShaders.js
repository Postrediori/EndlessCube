'use strict'

// Textured cube
const QUAD_VERTEX_SOURCE = `#version 100
precision highp float;

attribute vec3 a_position;
attribute vec2 a_texCoord;

uniform mat4 u_projection;
uniform mat4 u_view;

varying vec2 v_texCoord;

void main (void) {
    v_texCoord = a_texCoord;
    gl_Position = u_projection * u_view * vec4(a_position, 1.0);
}`;

const QUAD_FRAGMENT_SOURCE = `#version 100
precision highp float;

varying vec2 v_texCoord;

uniform sampler2D u_tex;
uniform vec4 u_color;

const float WindowRadius = 0.45;

void main (void) {
    if (length(v_texCoord-vec2(.5))>WindowRadius) {
        gl_FragColor = u_color;
    }
    else {
        gl_FragColor = texture2D(u_tex, v_texCoord);
    }
}`;


// Outline of the cube
const OUTLINE_VERTEX_SOURCE = `#version 100
precision highp float;

attribute vec3 a_position;

uniform mat4 u_projection;
uniform mat4 u_view;

void main (void) {
    gl_Position = u_projection * u_view * vec4(a_position, 1.0);
}`;

const OUTLINE_FRAGMENT_SOURCE = `#version 100
precision highp float;

uniform vec4 u_color;

void main (void) {
    gl_FragColor = u_color;
}`;
