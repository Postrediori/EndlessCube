'use strict'

class Camera {
    constructor() {
        this.azimuth = INITIAL_AZIMUTH;
        this.elevation = INITIAL_ELEVATION;

        this.limitAzimuth = false;
        this.limitElevation = true;

        this.viewMatrix = makeIdentityMatrix(new Float32Array(16));
        this.changed = true;

        this.orbitTranslationMatrix = makeIdentityMatrix(new Float32Array(16));
        this.xRotationMatrix = new Float32Array(16);
        this.yRotationMatrix = new Float32Array(16);
        this.distanceTranslationMatrix = makeIdentityMatrix(new Float32Array(16));

        this.orbitTranslationMatrix[12] = -ORBIT_POINT[0];
        this.orbitTranslationMatrix[13] = -ORBIT_POINT[1];
        this.orbitTranslationMatrix[14] = -ORBIT_POINT[2];
    }

    changeAzimuth(deltaAzimuth) {
        this.azimuth += deltaAzimuth;
        if (this.limitAzimuth) {
            this.azimuth = clamp(this.azimuth, MIN_AZIMUTH, MAX_AZIMUTH);
        }
        this.changed = true;
    };
    
    changeElevation(deltaElevation) {
        this.elevation += deltaElevation;
        if (this.limitElevation) {
            this.elevation = clamp(this.elevation, MIN_ELEVATION, MAX_ELEVATION);
        }
        this.changed = true;
    };

    getViewMatrix(distance = CAMERA_DISTANCE) {
        if (this.changed) {
            makeXRotationMatrix(this.xRotationMatrix, this.elevation);
            makeYRotationMatrix(this.yRotationMatrix, this.azimuth);

            this.changed = false;
        }

        makeIdentityMatrix(this.viewMatrix);
        this.distanceTranslationMatrix[14] = -distance;

        premultiplyMatrix(this.viewMatrix, this.viewMatrix, this.orbitTranslationMatrix);
        premultiplyMatrix(this.viewMatrix, this.viewMatrix, this.yRotationMatrix);
        premultiplyMatrix(this.viewMatrix, this.viewMatrix, this.xRotationMatrix);
        premultiplyMatrix(this.viewMatrix, this.viewMatrix, this.distanceTranslationMatrix);

        return this.viewMatrix;
    };
};
