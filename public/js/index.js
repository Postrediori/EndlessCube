'use strict'

function main() {
    let simulatorCanvas = document.getElementById(SIMULATOR_CANVAS_ID),
        overlayDiv = document.getElementById(OVERLAY_DIV_ID);

    let gl = simulatorCanvas.getContext('webgl') || simulatorCanvas.getContext('experimental-webgl');
    if (!gl) {
        console.log("No WebGL");
        return;
    }
    console.log(`WebGL Renderer : ${gl.getParameter(gl.RENDERER)}`);
    console.log(`WebGL Vendor : ${gl.getParameter(gl.VENDOR)}`);
    console.log(`WebGL Version : ${gl.getParameter(gl.VERSION)}`);
    console.log(`WebGL GLSL Version : ${gl.getParameter(gl.SHADING_LANGUAGE_VERSION)}`);

    let simulator = new Simulator(gl, simulatorCanvas, 640, 480);

    let camera = new Camera();
    let projectionMatrix = makePerspectiveMatrix(new Float32Array(16), FOV, MIN_ASPECT, NEAR, FAR);

    let width = window.innerWidth,
        height = window.innerHeight;

    // Mouse interface
    let lastMouseX = 0;
    let lastMouseY = 0;
    let mode = CAMERA_MODE_NONE;

    let onMouseDown = function (event) {
        event.preventDefault();

        let mousePosition = getMousePosition(event, overlayDiv);
        let mouseX = mousePosition.x,
            mouseY = mousePosition.y;

        mode = CAMERA_MODE_ORBITING;
        lastMouseX = mouseX;
        lastMouseY = mouseY;
    };
    overlayDiv.addEventListener('mousedown', onMouseDown, false);

    overlayDiv.addEventListener('mousemove', function (event) {
        event.preventDefault();

        let mousePosition = getMousePosition(event, overlayDiv),
            mouseX = mousePosition.x,
            mouseY = mousePosition.y;

        if (mode === CAMERA_MODE_ORBITING) {
            overlayDiv.style.cursor = '-webkit-grabbing';
            overlayDiv.style.cursor = '-moz-grabbing';
            overlayDiv.style.cursor = 'grabbing';
        }
        else {
            overlayDiv.style.cursor = '-webkit-grab';
            overlayDiv.style.cursor = '-moz-grab';
            overlayDiv.style.cursor = 'grab';
        }

        if (mode === CAMERA_MODE_ORBITING) {
            camera.changeAzimuth((mouseX - lastMouseX) / width * CAMERA_SENSITIVITY);
            camera.changeElevation((mouseY - lastMouseY) / height * CAMERA_SENSITIVITY);
            lastMouseX = mouseX;
            lastMouseY = mouseY;
        }
    });

    overlayDiv.addEventListener('mouseup', function (event) {
        event.preventDefault();
        mode = CAMERA_MODE_NONE;
    });

    window.addEventListener('mouseout', function (event) {
        let from = event.relatedTarget || event.toElement;
        if (!from || from.nodeName === 'HTML') {
            mode = CAMERA_MODE_NONE;
        }
    });

    // Scroll events
    let zoom = CAMERA_DISTANCE;

    window.addEventListener('wheel', function (event) {
        zoom += event.deltaY * ZOOM_SENSITIVITY;
        zoom = clamp(zoom, MIN_ZOOM, MAX_ZOOM);
    });

    // Touch interface
    let touchRotate = false;
    let touchScale = false;
    let scaleInitialDist = 0.0;
    let scaleInitialZoom = zoom;
    let scaleFactor = 0.0;

    let scaleStart = function(e) {
        if (e.touches.length !== 2) {
            console.log('Error: scale requires 2 touch points');
            return;
        }

        scaleInitialDist = Math.hypot(
            e.touches[0].pageX - e.touches[1].pageX,
            e.touches[0].pageY - e.touches[1].pageY);
        scaleInitialZoom = zoom;
    };
    
    let scaleEnd = function(e) {
        zoom = scaleInitialZoom * scaleFactor;
        zoom = clamp(zoom, MIN_ZOOM, MAX_ZOOM);
    };

    let scaleMove = function(e) {
        if (e.touches.length !== 2) {
            console.log('Error: scale requires 2 touch points');
            return;
        }

        const dist = Math.hypot(
            e.touches[0].pageX - e.touches[1].pageX,
            e.touches[0].pageY - e.touches[1].pageY);

        scaleFactor = scaleInitialDist / dist;
        zoom = scaleInitialZoom * scaleFactor;
        zoom = clamp(zoom, MIN_ZOOM, MAX_ZOOM);
    };

    overlayDiv.addEventListener('touchstart', function (event) {
        event.preventDefault();

        const touches = event.changedTouches;
        if (touches.length === 1) {
            touchRotate = true;

            if (mode === CAMERA_MODE_NONE) {
                mode = CAMERA_MODE_ORBITING;

                const touch = touches[0];
                lastMouseX = touch.pageX;
                lastMouseY = touch.pageY;
            }
        }
        else if (touches.length === 2) {
            touchScale = true;
            scaleStart(event);
        }
    });

    overlayDiv.addEventListener('touchend', function (event) {
        event.preventDefault();
        
        if (touchRotate) {
            touchRotate = false;
            if (mode === CAMERA_MODE_ORBITING) {
                mode = CAMERA_MODE_NONE;
            }
        }
        else if (touchScale) {
            touchScale = false;
            scaleEnd(event);
        }
    });

    overlayDiv.addEventListener('touchcancel', function (event) {
        event.preventDefault();
        
        if (touchRotate) {
            touchRotate = false;
            if (mode === CAMERA_MODE_ORBITING) {
                mode = CAMERA_MODE_NONE;
            }
        }
        else if (touchScale) {
            touchScale = false;
            scaleEnd(event);
        }
    });

    overlayDiv.addEventListener('touchmove', function (event) {
        event.preventDefault();

        const touches = event.changedTouches;

        if (touchRotate) {
            const touch = touches[0];
            const mouseX = touch.pageX,
                mouseY = touch.pageY;

            if (mode === CAMERA_MODE_ORBITING) {
                camera.changeAzimuth((mouseX - lastMouseX) / width * CAMERA_SENSITIVITY);
                camera.changeElevation((mouseY - lastMouseY) / height * CAMERA_SENSITIVITY);
                lastMouseX = mouseX;
                lastMouseY = mouseY;
            }
        }
        else if (touchScale) {
            scaleMove(event);
        }
    });

    // Resize
    let onresize = function (event) {
        let windowWidth = window.innerWidth,
            windowHeight = window.innerHeight;

        overlayDiv.style.width = `${windowWidth}px`;
        overlayDiv.style.height = `${windowHeight}px`;

        if (windowWidth / windowHeight > MIN_ASPECT) {
            makePerspectiveMatrix(projectionMatrix, FOV, windowWidth / windowHeight, NEAR, FAR);
            simulator.resize(windowWidth, windowHeight);
            simulatorCanvas.style.top = `${0}px`;
        } else {
            let newHeight = windowWidth / MIN_ASPECT;
            makePerspectiveMatrix(projectionMatrix, FOV, windowWidth / newHeight, NEAR, FAR);
            simulator.resize(windowWidth, newHeight);
            simulatorCanvas.style.top = `${(windowHeight-newHeight)*0.5}px`;
        }
    };

    window.addEventListener('resize', onresize);
    onresize();

    let lastTime = (new Date()).getTime();
    let render = function render (currentTime) {
        let deltaTime = (currentTime - lastTime) / 1000 || 0.0;
        lastTime = currentTime;

        simulator.render(projectionMatrix, camera, zoom);

        requestAnimationFrame(render);
    };
    render();
}

main();
