'use strict'

class OffscreenRenderer {
    constructor(gl) {
        this.gl = gl;

        this.width = 1;
        this.height = 1;

        this.renderBuffer = gl.createRenderbuffer();
        gl.bindRenderbuffer(gl.RENDERBUFFER, this.renderBuffer);
        gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_STENCIL, this.width, this.height);
        
        this.framebuffer = buildFramebuffer(gl);
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.framebuffer);
        gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_STENCIL_ATTACHMENT,
                                gl.RENDERBUFFER, this.renderBuffer);
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    }

    resize(width, height) {
        let gl = this.gl;

        this.width = width;
        this.height = height;

        gl.bindRenderbuffer(gl.RENDERBUFFER, this.renderBuffer);
        gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_STENCIL, this.width, this.height);
    }

    renderStart(dstTexture) {
        let gl = this.gl;

        gl.bindFramebuffer(gl.FRAMEBUFFER, this.framebuffer);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, dstTexture, 0);
    }

    renderEnd() {
        let gl = this.gl;

        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    }
}

class TexturedCubeMesh {
    constructor(gl) {
        this.gl = gl;

        this.quadProgram = buildProgramWrapper(gl,
            buildShader(gl, gl.VERTEX_SHADER, QUAD_VERTEX_SOURCE),
            buildShader(gl, gl.FRAGMENT_SHADER, QUAD_FRAGMENT_SOURCE),
            {"a_position" : 0, "a_texCoord": 1});

        this.outlineProgram = buildProgramWrapper(gl,
            buildShader(gl, gl.VERTEX_SHADER, OUTLINE_VERTEX_SOURCE),
            buildShader(gl, gl.FRAGMENT_SHADER, OUTLINE_FRAGMENT_SOURCE),
            {"a_position" : 0});

        gl.enableVertexAttribArray(0);
        gl.enableVertexAttribArray(1);

        this.cubeBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, CUBE_VERTICES, gl.STATIC_DRAW);

        this.cubeIndexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.cubeIndexBuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, CUBE_INDICES, gl.STATIC_DRAW);
        this.cubeIndexBufferLength = CUBE_INDICES.length;

        this.cubeOutlineIndexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.cubeOutlineIndexBuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, CUBE_OUTLINE_INDICES, gl.STATIC_DRAW);
        this.cubeOutlineIndexBufferLength = CUBE_OUTLINE_INDICES.length;
    }

    render(texture, projectionMatrix, viewMatrix) {
        let gl = this.gl;

        // Draw Cube Faces
        gl.useProgram(this.quadProgram.program);

        gl.polygonOffset(1, 0);
        gl.enable(gl.POLYGON_OFFSET_FILL);

        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.activeTexture(gl.TEXTURE0);
        gl.uniform1i(this.quadProgram.uniformLocations['u_tex'], SCREEN_TEXTURE_UNIT);

        gl.uniform4fv(this.quadProgram.uniformLocations['u_color'], OUTLINE_COLOR);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeBuffer);
        gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 5 * SIZE_OF_FLOAT, 0);
        gl.vertexAttribPointer(1, 2, gl.FLOAT, false, 5 * SIZE_OF_FLOAT, 3 * SIZE_OF_FLOAT);

        gl.uniformMatrix4fv(this.quadProgram.uniformLocations['u_projection'], false, projectionMatrix);
        gl.uniformMatrix4fv(this.quadProgram.uniformLocations['u_view'], false, viewMatrix);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.cubeIndexBuffer);
        gl.drawElements(gl.TRIANGLES, this.cubeIndexBufferLength, gl.UNSIGNED_SHORT, 0);

        // Draw Cube Edges
        gl.useProgram(this.outlineProgram.program);

        gl.polygonOffset(0, 0);
        gl.disable(gl.POLYGON_OFFSET_FILL);

        gl.uniform4fv(this.outlineProgram.uniformLocations['u_color'], EDGE_COLOR);

        gl.uniformMatrix4fv(this.outlineProgram.uniformLocations['u_projection'], false, projectionMatrix);
        gl.uniformMatrix4fv(this.outlineProgram.uniformLocations['u_view'], false, viewMatrix);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeBuffer);
        gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 5 * SIZE_OF_FLOAT, 0);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.cubeOutlineIndexBuffer);
        gl.drawElements(gl.LINES, this.cubeOutlineIndexBufferLength, gl.UNSIGNED_SHORT, 0);
    }
};

// Main class
class Simulator { constructor(gl, canvas, width, height) {
    this.canvas = canvas;
    this.gl = gl;

    this.distance = 1.0;

    gl.clearColor.apply(gl, CLEAR_COLOR);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    this.cubeMesh = new TexturedCubeMesh(gl);

    // Textures
    this.textures = [
        buildTexture(gl, gl.RGBA, gl.UNSIGNED_BYTE, gl.CLAMP_TO_EDGE, gl.CLAMP_TO_EDGE, gl.LINEAR, gl.LINEAR),
        buildTexture(gl, gl.RGBA, gl.UNSIGNED_BYTE, gl.CLAMP_TO_EDGE, gl.CLAMP_TO_EDGE, gl.LINEAR, gl.LINEAR)
    ];
    this.srcTexture = 0;

    this.offscreenRenderer = new OffscreenRenderer(gl);
    
    const SQUARE_ASPECT = 1 / 1;
    this.projectionMatrix = makePerspectiveMatrix(new Float32Array(16), FOV, SQUARE_ASPECT, NEAR, FAR);

    // Initial resize
    this.resize(width, height);
}

    resize(width, height) {
        let gl = this.gl;

        this.canvas.width = width;
        this.canvas.height = height;

        this.resolution = Math.min(width, height);

        for (let i = 0; i < this.textures.length; i++) {
            gl.bindTexture(gl.TEXTURE_2D, this.textures[i]);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA,
                      this.resolution, this.resolution, 0, gl.RGBA, gl.UNSIGNED_BYTE,
                      null);
        }

        this.offscreenRenderer.resize(this.resolution, this.resolution);
    };

    render(projectionMatrix, cameraObj, zoom) {
        let gl = this.gl

        let dstTexture = (~this.srcTexture) & 1;

        // Render cube with texture and outline to virtual framebuffer
        this.offscreenRenderer.renderStart(this.textures[dstTexture]);

        gl.viewport(0, 0, this.resolution, this.resolution);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        this.cubeMesh.render(this.textures[this.srcTexture], this.projectionMatrix, cameraObj.getViewMatrix());

        this.offscreenRenderer.renderEnd();

        // Screen shader
        gl.viewport(0, 0, this.canvas.width, this.canvas.height);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        this.cubeMesh.render(this.textures[this.srcTexture], projectionMatrix, cameraObj.getViewMatrix(zoom));

        // Swap textures
        this.srcTexture = dstTexture;
    }
}
